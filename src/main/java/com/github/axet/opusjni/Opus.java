package com.github.axet.opusjni;

public class Opus {
    static {
        if (Config.natives) {
            System.loadLibrary("opusjni");
        }
    }

    private long handle;

    public native void open(int channels, int sampleRate, int brate);

    public native byte[] encode(short[] buf, int pos, int len);

    public native void close();

}
